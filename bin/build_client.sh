#!/bin/bash

LIBS=$PWD/../lib/

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIBS/SFML-2.2/lib/:$LIBS/

cd ../src/client
make
