#!/bin/bash

while [ "$1" != "" ]; do
	case $1 in
		-c )	shift
					cd $PWD/../src/server
					mix run --no-halt
					;;
		-g )	shift
					gnome-terminal --working-directory=$PWD/../src/server \
						-- mix run --no-halt
					;;
		-m ) 	shift
					mate-terminal --working-directory=$PWD/../src/server \
						--command "mix run --no-halt"
					;;
	esac
	shift
done
