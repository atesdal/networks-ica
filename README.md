# NCP ICA Teesside University
## C++(SFML) client and Elixir server/router

This is the repository for my NCP ICA, 2nd semester of Computer Games
programming at Teesside University. I'm also using this as an opportunity to
learn more about the Project and Issue tracker functionality in GitHub, in
addition to also managing the whole project using VIM/Makefiles/bash scripts.

Because of this some of the structure might be inefficient, but hopefully I
will learn a lot using this approach.

## Server

Run server in current terminal:

```
cd bin/
./run_server.sh -c
```

Run server in a seperate gnome/mate terminal:

```
cd bin/
./run_server.sh -g/m
```

-g: Opens a new gnome-terminal running the server

-m: Opens a new mate-terminal running the server

You can also run it manually:

```
cd src/server
mix run --no-halt
```

The code for the server can be found in `src/server/lib`

## Client

To build the client:

```
cd src/client
make
```

To run the client:

```
cd bin/
./run_client.sh
```

Or

```
cd src/client
./run.sh -c
```

Client code can be found in `src/client/src` and the headers in
`src/client/include`.

This client is built using SFML-2.2, which can be downloaded from 
`https://www.sfml-dev.org/download/sfml/2.2/`, extract the contents and put the
`SFML-2.2` directory in lib/

I also noticed at the start of this project that the most current g++ compiler 
will not build this. I resolved this by installing g++4.9 alongside my standard
arch gcc/g++ installation. If your client crashes due to free(): invalid pointer
errors when you run it please check your version.

The client can be run on a different machine as long as the ip address
matches that of the server (127.0.0.1, 13000(TCP), 13001(UDP) by default)
