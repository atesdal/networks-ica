defmodule Server do
  use Application

  def start(_type, _args) do
    IO.puts("Server starting on ports 13000(TCP) and 13001(UDP)")
    Task.start_link(Router, :start, [])
    Task.start_link(Tcp, :start, [13000])
    Task.start_link(Udp, :start, [13001])
  end
end

defmodule Tcp do
  @moduledoc """
  A module to open a TCP socket, accept any connection and
  echo back any binary message received.
  """

  @doc """
  Creates a TCP socket and starts listening for connections.
  """
  def start(port) do
    options = [:binary, reuseaddr: true, active: false, backlog: 5]
    {:ok, lsocket} = :gen_tcp.listen(port, options)
    accept_loop(lsocket)
  end

  @doc """
  Accept any incoming connection. We spawn a new process everytime.
  """
  def accept_loop(lsocket) do
    case :gen_tcp.accept(lsocket) do
      {:ok, socket} ->
        spawn(fn -> client_loop(socket) end)
        accept_loop(lsocket)
      {:error, reason} ->
        IO.puts "tcp accept failed on:"
        IO.inspect(reason)
    end
  end

  @doc """
  client_loop tries to receive as much data as possible and
  echoes it back to the sender.
  """
  def client_loop(socket) do
    case :gen_tcp.recv(socket, 0) do
      {:ok, message} ->
        messages = Message.split(message)
        #IO.inspect(messages);
        case handle(messages, socket) do
          :success ->
            client_loop(socket)
          _ ->
            :gen_tcp.close(socket)
            IO.puts "Handle failed"
        end
        #        case Message.interpret(msg_parts) do
        #          {:register, name, posX, posY} ->
        #            Router.register(name, posX, posY, socket)
        #            client_loop(socket)
        #          {:shout, sender, message} ->
        #            Router.shout(sender, message)
        #            client_loop(socket)
        #          {:whisper, sender, recipient, message} ->
        #            Router.whisper(sender, recipient, message)
        #            client_loop(socket)
        #          {:move, name, posX, posY} ->
        #            Router.move(name, posX, posY)
        #            client_loop(socket)
        #          {:undefined, other} ->
        #            IO.puts "Couldn't interpret: '#{inspect other}'"
        #            client_loop(socket)
        #end
      otherwise ->
        IO.inspect(otherwise)
    end
  end

  defp handle([message | t], socket) do
    case Message.interpret(message) do
      {:register, name, posX, posY} ->
        Router.register(name, posX, posY, socket)
        handle(t, socket)
      {:collision, name1, name2} ->
        Router.collide(name1, name2)
        :quit
      {:shout, sender, message} ->
        Router.shout(sender, message)
        handle(t, socket)
      {:whisper, sender, recipient, message} ->
        Router.whisper(sender, recipient, message)
        handle(t, socket)
      {:undefined, other} ->
        {:error, other}
    end
  end
  defp handle([], _) do
    :success
  end
end

defmodule Udp do
  @moduledoc """
  Opens a UDP socket and use it to send back any message received.
  """

  @doc """
  Opens a UDP socket.
  """
  def start(port) do
    options = [:binary, reuseaddr: true, active: false]
    {:ok, socket} = :gen_udp.open(port, options)
    recv_loop(socket)
  end

  @doc """
  recv_loop receives as much data as possible and sends it
  back to the sender.
  """
  def recv_loop(socket) do
    case :gen_udp.recv(socket, 0) do
      {:ok, {_address, _port, message}} ->
        messages = Message.split(message)
        case handle(messages) do
          :success ->
            recv_loop(socket)
          _ ->
            IO.puts "UDP recieve failed"
        end
        #IO.puts "Msg(UPD) in: #{inspect msg_parts}"
        # case Message.interpret(msg_parts) do
        #   {:unregister, name} ->
        #     #Obviously this is not good as a final product since there is no
        #     #verification, so anyone could unregister anyone, but in its
        #     #state this should be OK
        #     Router.unregister(name)
        #     recv_loop(socket)
        #   {:move, name, posX, posY} ->
        #     Router.move(name, posX, posY)
        #     recv_loop(socket)
        #   {:quit, name} ->
        #     Router.quit(name)
        #     recv_loop(socket)
        #   {:undefined, other} ->
        #     IO.puts "Couldn't interpret: '#{inspect other}'"
        # end
      {:error, reason} ->
        IO.inspect(reason)
    end
  end

  defp handle([message | t]) do
    case Message.interpret(message) do
      {:unregister, name} ->
        Router.unregister(name)
        handle(t)
      {:move, name, posX, posY} ->
        Router.move(name, posX, posY)
        handle(t)
      {:quit, name} ->
        Router.quit(name)
        handle(t)
      {:undefined, other} ->
        {:error, other}
    end
  end
  defp handle([]) do
    :success
  end

end
