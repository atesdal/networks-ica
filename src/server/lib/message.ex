defmodule Message do

  def split(message) do
    messages = message |> String.trim |> String.split(";", trim: true)
    f = fn msg -> (msg |> String.trim |> String.split("|", trim: true)) end
    Enum.map(messages, f)
    # [[msg1] | [msg2]]
  end

  #interpreting incoming messages
  def interpret(message) do
    case message do
      #expected format: register|name|posX|posY
      ["register", name, posX, posY] ->
        {:register, name, posX, posY}
        # {:register, Enum.at(list, 0), Enum.at(list, 1), Enum.at(list, 2)}
      #expected format: unregister|name
      ["unregister", name] ->
        {:unregister, name}
      #expected format: shout|sender|message
      ["shout", sender, message] ->
        {:shout, sender, message}
      #expected format: whisper|sender|recipient|message
      ["whisper", sender, recipient, message] ->
        {:whisper, sender, recipient, message}
      #expected format: move|name|posX|posY
      ["move", name, posX, posY] ->
        {:move, name, posX, posY}
      #expected format: collision|name1|name2
      ["collision", name1, name2] ->
        {:collision, name1, name2}
      #expected format: quit|name
      ["quit", name] ->
        {:quit, name}
      #expected format: shutdown
      ["shutdown"] ->
        :shutdown
      [other] ->
        {:undefined, other}
    end
  end

  #these functions are for formatting outgoing messages so they can be
  #interpreted by the ReturnMessage class
  def confirmation(message) do
    "c" <> message
  end

  def error(message) do
    "e" <> message
  end

  def reg(name, posX, posY) do
    "a" <> name <> "|" <> posX <> "|" <> posY
  end

  def unreg(name) do
    "r" <> name
  end

  def quit() do
    "q"
  end

  def move(name, posX, posY) do
    "m" <> name <> "|" <> posX <> "|" <> posY
  end

  def whisper(sender, message) do
    "w" <> message <> "|" <> sender
  end
end
