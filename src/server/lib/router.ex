defmodule Router do
  
  @router :router

  def start() do
    :global.trans({@router, @router}, fn ->
      case :global.whereis_name(@router) do
        :undefined ->
          pid = spawn(Router, :route_msg, [%{}])
          :global.register_name(@router, pid)
        _ ->
          :ok
      end
    end)
  end

  def stop() do
    :global.trans({@router, @router}, fn ->
      case :global.whereis_name(@router) do
        :undefined ->
          :ok
        _ ->
          :global.send @router, :shutdown
      end
    end)
  end

  def route_msg(clients) do
    IO.puts "Clients: #{inspect clients}"
    receive do
      msg ->
        case handle(msg, clients) do
          {:no_reply, new_clients} ->
            route_msg(new_clients)
          :shutdown ->
            IO.puts "Router #{inspect self()} shutting down."
        end
    end
  end

  def handle({:register, sender, posX, posY, socket}, clients) do
    case Map.fetch(clients, sender) do
      {:ok, _} ->
        IO.puts "Name already registered."
        :gen_tcp.send(socket, Message.error("Name already registered"))
        {:no_reply, clients}
      :error ->
        Enum.each(clients, fn({name, {_, posX, posY}}) ->
          if(name !== sender) do
            :gen_tcp.send(socket, Message.reg(name, posX, posY))
          end
        end)
        new_clients = Map.put(clients, sender, {socket, posX, posY})
        server_broadcast(sender, Message.reg(sender, posX, posY))
        {:no_reply, new_clients}
    end
  end

  def handle({:unregister, sender}, clients) do
    case Map.fetch(clients, sender) do
      {:ok, _} ->
        new_clients = Map.delete(clients, sender)
        server_broadcast(sender, Message.unreg(sender))
        {:no_reply, new_clients}
      :error ->
        IO.puts "Client #{sender} is not registered."
    end
  end

  def handle({:shout, _sender, _message}, clients) do
    #currently unused, maybe some other day, planned to use UDP to send
    #message to everyone it could
    {:no_reply, clients}
  end

  def handle({:whisper, sender, recipient, message}, clients) do
    #currently unused, but for some reason I spent time implementing it anyway
    case clients[recipient] do
      nil ->
        IO.puts "Client #{recipient} is not registered."
        #send error to client
        {:no_reply, clients}
      {recipient_socket, _, _} ->
        IO.puts "Sending message: #{inspect message}"
        :gen_tcp.send(recipient_socket, Message.whisper(sender, message))
        {:no_reply, clients}
    end
  end

  def handle({:move, sender, posX, posY}, clients) do
    case Map.fetch(clients, sender) do
      {:ok, _} ->
        server_broadcast(sender, Message.move(sender, posX, posY))
        {socket, _, _} = clients[sender]
        new_clients = Map.put(clients, sender, {socket, posX, posY})
        {:no_reply, new_clients}
      :error ->
        IO.puts "Trying to send move with unregistered client"
        {:no_reply, clients}
    end
  end

  def handle({:collision, name1, name2}, clients) do
    case clients[name1] do
      {socket, _, _} ->
        :gen_tcp.send(socket, Message.quit)
    end
    case clients[name2] do
      {socket, _, _} ->
        :gen_tcp.send(socket, Message.quit)
    end
    {:no_reply, clients}
  end

  def handle({:server_broadcast, sender, message}, clients) do
    Enum.each(clients, fn({name, {socket, _, _}}) ->
      if name !== sender, do: :gen_tcp.send(socket, message)
    end)
    {:no_reply, clients}
  end

  def handle({:quit, sender}, clients) do
    IO.inspect(sender)
    case clients[sender] do
      nil ->
        IO.puts "Client #{sender} is not registered."
        {:no_reply, clients}
      {client_socket, _, _} ->
        IO.puts "Client #{inspect sender} is leaving the server :("
        :gen_tcp.send(client_socket, "shutdown")
        unregister(sender)
        {:quit, sender}
    end
  end

  def handle(other, clients) do
    IO.puts "Undefined router atom #{inspect other}"
    {:no_reply, clients}
  end

  def handle(:shutdown) do
    :shutdown 
  end

  def register(sender, posX, posY, socket) do
    :global.send(@router, {:register, sender, posX, posY, socket})
  end

  def unregister(sender) do
    :global.send(@router, {:unregister, sender})
  end

  def shout(sender, message) do
    :global.send(@router, {:shout, sender, message})
  end

  def collide(name1, name2) do
    :global.send(@router, {:collision, name1, name2})
  end

  def whisper(sender, recipient, message) do
    :global.send(@router, {:whisper, sender, recipient, message})
  end

  def move(sender, posX, posY) do
    :global.send(@router, {:move, sender, posX, posY})
  end

  def server_broadcast(sender, message) do
    :global.send(@router, {:server_broadcast, sender, message})
  end

  def quit(sender) do
    :global.send(@router, {:quit, sender})
  end

  def shutdown() do
    :global.send(@router, :shutdown)
  end
end
