#include "Utils.hpp"
#include <iomanip>
#include <sstream>

namespace Util
{
	std::string ToString(float toConvert)
	{
		std::ostringstream ss;
		ss << std::fixed << std::setprecision(2) << toConvert;
		std::string s = ss.str();
		return s;
	}
}
