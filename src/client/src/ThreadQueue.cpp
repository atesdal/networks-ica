#include "ThreadQueue.hpp"
#include <utility>

ThreadQueue::ThreadQueue()
{

}

ThreadQueue::~ThreadQueue()
{

}

void ThreadQueue::Push(Message &message)
{
	std::lock_guard<std::mutex> lock(m_);
	messageBuffer_.push(message);
	c_.notify_one();
}

void ThreadQueue::Pop(Message &message)
{
	std::unique_lock<std::mutex> lock(m_);
	while(messageBuffer_.empty()){
		c_.wait(lock);
	}
	message = messageBuffer_.front();
	messageBuffer_.pop();
}

bool ThreadQueue::TryPop(Message &message)
{
	std::unique_lock<std::mutex> lock(m_);
	if(messageBuffer_.empty()){
		return false;
	}
	message = messageBuffer_.front();
	messageBuffer_.pop();
	return true;
}

