#include "ReturnMessage.hpp"
#include <sstream>
#include <iostream>

ReturnMessage::ReturnMessage() :
	Message()
{
}

ReturnMessage::~ReturnMessage()
{
}

void ReturnMessage::FillMessage(std::string &message)
{
	Format(message);
}

void ReturnMessage::Format(std::string &rawMessage)
{
	//Need to add some way of differentiating between different messages,
	//ie. is it a position update, shooting, etc.
	//Think I will add an int at the start of message from server that can
	//identify the message type to this class, which can then set its
	//type accordingly.
	
	char identifier = rawMessage.at(0);

	switch(identifier) {
		case 'a':
			t_ = Type::Register;
			command_ = "register";
			break;
		case 'r':
			t_ = Type::Unregister;
			command_ = "unregister";
			break;
		case 'm':
			t_ = Type::Move;
			command_ = "move";
			break;
		case 'c':
			t_ = Type::Confirmation;
			command_ = "success";
			break;
		case 'e':
			t_ = Type::Error;
			command_ = "error";
			break;
		case 'w':
			t_ = Type::Whisper;
			command_ = "whisper";
			break;
		case 'q':
			t_ = Type::Quit;
			command_ = "quit";
			break;
		default:
			std::cerr << "Undefined return type: " << identifier << std::endl;
			t_ = Type::Undefined;
			break;
	}
	
	rawMessage.erase(rawMessage.begin());

	std::string argument;
	std::istringstream iss(rawMessage);

	while(std::getline(iss, argument, separator_)) {
		args_.push_back(argument.c_str());
	}
}

