#include "Network.hpp"
#include "ReturnMessage.hpp"
#include "Utils.hpp"
#include "ThreadQueue.hpp"
#include <iostream>
#include <functional>
#include <cstring>

Network::Network() :
	servTCP_(13000), servUDP_(13001)
{
}

Network::~Network()
{
	if(connected_) {
		Disconnect();
		tcpThread_.detach();
		udpThread_.detach();
	}
}

bool Network::Connect(sf::IpAddress ip,	unsigned short int port)
{
	servIP_ = ip;
	sf::Socket::Status status = TCPSock_.connect(ip, port);
	if(status == sf::Socket::Error){
		std::cerr << "connection failed\n";
		return false;
	}
	connected_ = true;
	return true;
}

void Network::Disconnect()
{
	Message msg(Message::Type::Unregister, clientName_);
	SendUDP(msg);
}

void Network::Register(std::string name, float posX, float posY)
{
	clientName_ = name;
	Message reg(Message::Type::Register, name);
	reg.AddArg(Util::ToString(posX));
	reg.AddArg(Util::ToString(posY));
	SendTCP(reg);
}

void Network::SendTCP(Message &msg)
{
	std::string bufferOut = msg.GetData();
	std::cout << "Sending : " << bufferOut << std::endl;
	if(TCPSock_.send(bufferOut.c_str(), bufferOut.size())
			!= sf::Socket::Done)
	{
		std::cerr << "TCP send failed";
	}
}

void Network::SendUDP(Message &msg)
{
	std::string bufferOut = msg.GetData();
	if(UDPSock_.send(
			bufferOut.c_str(),
			bufferOut.size(),
			servIP_,
			servUDP_)
		!= sf::Socket::Done)
	{
		std::cerr << "UDP send failed";
	}
}

void Network::StartReceive(ThreadQueue &messageQ)
{
	tcpThread_ = std::thread(
			&Network::ReceiveTCP,
			this,
			std::ref(messageQ));

	udpThread_ = std::thread(
			&Network::ReceiveUDP,
			this,
			std::ref(messageQ));
}

void Network::ReceiveTCP(ThreadQueue &q)
{
	std::cout << "Starting TCP thread" << std::endl;
	char bufferIn[1024];
	std::string bufferString;
	std::size_t received;

	while(bufferString != "quit") {
		
		memset(bufferIn, 0, sizeof(bufferIn));
		if(TCPSock_.receive(
					bufferIn,
					100,
					received)
				!= sf::Socket::Done)
		{
			//std::cerr << "TCP Receive failed" << std::endl;
		}
		else {
			ReturnMessage rMsg;
			bufferString = bufferIn;
			rMsg.FillMessage(bufferString);
			if(rMsg.GetType() != Message::Type::Undefined) {
				q.Push(rMsg);
			}
		}
	}
	std::cout << "TCP thread dead" << std::endl;
}

void Network::ReceiveUDP(ThreadQueue &q)
{
	std::cout << "Starting UDP thread" << std::endl;
	char bufferIn[1024];
	std::string bufferString;
	std::size_t received;
  sf::IpAddress sender;
  unsigned short senderPort;

	while(bufferString != "quit") {

		memset(bufferIn, 0, sizeof(bufferIn));
		if(UDPSock_.receive(
					bufferIn,
					100,
					received,
					sender,
					senderPort)
				!= sf::Socket::Done)
		{
		}
		else {
			ReturnMessage rMsg;
			bufferString = bufferIn;
			rMsg.FillMessage(bufferString);
			q.Push(rMsg);
		}
	}
	std::cout << "UDP thread dead" << std::endl;
}


