/*
  Snake ICA start project using SFML
    SFML documentation: http://www.sfml-dev.org/documentation/2.4.2/classes.php
  Windows build
    To build on Windows you need to have the correct version of CodeBlocks
      Download from here: http://www.codeblocks.org/downloads/26
      Must be the download titled "codeblocks-16.01mingw-setup.exe"
*/

#include <iostream>
#include <cstring>
#include <thread>
#include <functional>

#include "Game.hpp"

int main()
{
	std::string input;
	std::cin >> input;

	Game g(input);

	g.run();

  return 0;
}
