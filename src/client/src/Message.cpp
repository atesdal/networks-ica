#include "Message.hpp"
#include <iostream>

Message::Message()
{
}

Message::Message(Type messageType) :
	t_(messageType)
{
	switch(messageType) {
		case Type::Register:
			command_ = "register";
			break;
		case Type::Unregister:
			command_ = "unregister";
			break;
		case Type::Move:
			command_ = "move";
			break;
		case Type::Collision:
			command_ = "collision";
			break;
		default:
			std::cerr << "Undefined type in message constructor.\n";
			break;
	}
}

Message::Message(Type messageType, std::string arg) :
	t_(messageType)
{
	switch(messageType) {
		case Type::Register:
			command_ = "register";
			break;
		case Type::Unregister:
			command_ = "unregister";
			break;
		case Type::Move:
			command_ = "move";
			break;
		case Type::Collision:
			command_ = "collision";
			break;
		default:
			std::cerr << "Undefined type in message constructor.\n";
			break;
	}
	AddArg(arg);
}

Message::~Message()
{
}

std::string Message::GetData() const
{
	std::string fullMessage = command_;
	for(auto &p : args_) {
		fullMessage += separator_ + p;
	}

	fullMessage += eol_;

	return fullMessage;
}

Message::Type Message::GetType() const
{
	return t_;
}

std::string Message::GetArg(int index)
{
	return args_[index];
}

std::vector<std::string> Message::GetArgs() const
{
	return args_;
}

void Message::AddArg(std::string newArg)
{
	args_.push_back(newArg);
}

void Message::ClearArgs()
{
	args_.resize(0);
	args_.clear();
}
