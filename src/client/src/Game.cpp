#include "Game.hpp"
#include "Stringhelpers.h"
#include "Utils.hpp"
#include "Network.hpp"
#include "Message.hpp"

#include <stdlib.h>
#include <iostream>

const float Game::PlayerSpeed = 100.f;
const sf::Time Game::TimePerFrame = sf::seconds(1.f/60.f);

Game::Game(std::string name) :
	mWindow(sf::VideoMode(640, 480), "SFML Application", sf::Style::Close),
	mTexture(),
	mFont(),
	mStatisticsText(),
	mStatisticsUpdateTime(),
	mStatisticsNumFrames(0),
	mIsMovingUp(false),
	mIsMovingDown(false),
	mIsMovingRight(false),
	mIsMovingLeft(false),
	playerName_(name)
{
	if (!mTexture.loadFromFile("Media/Textures/Eagle.png"))
	{
		// Handle loading error
	}
	
	sf::Sprite mPlayer;
	mPlayer.setTexture(mTexture);
	mPlayer.setPosition(100.f, 100.f);

	Player p;
	p.sprite = mPlayer;
	p.width = 48;
	p.height = 64;
	playerMap_[name] = p;
	
	mFont.loadFromFile("Media/Sansation.ttf");
	mStatisticsText.setFont(mFont);
	mStatisticsText.setPosition(5.f, 5.f);
	mStatisticsText.setCharacterSize(10);
	
	
	network_ = new Network();
	connected_ = network_->Connect("152.105.21.135", 13000);
	if(connected_) {
		network_->Register(name, p.GetX(), p.GetY());
		network_->StartReceive(messageBuffer_);
	}
	else {
		std::cout << "Unable to connect to server at 127.0.0.1, port 13000"
			<< std::endl;
		delete network_;
	}
}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (mWindow.isOpen())
	{
	  sf::Time elapsedTime = clock.restart();
	  timeSinceLastUpdate += elapsedTime;
	  while (timeSinceLastUpdate > TimePerFrame)
	  {
	      timeSinceLastUpdate -= TimePerFrame;
	
	      processEvents();
	      update(TimePerFrame);
	  }
	
	  updateStatistics(elapsedTime);
	  render();
		if(connected_) {
			Message updateMessage(Message::Type::Move, playerName_);
			updateMessage.AddArg(Util::ToString(
						playerMap_.at(playerName_).GetX()));
			updateMessage.AddArg(Util::ToString(
						playerMap_.at(playerName_).GetY()));
			network_->SendUDP(updateMessage);
		}
	}
}

void Game::processEvents()
{
	if(connected_) {
		Message m;
		while(messageBuffer_.TryPop(m)) {
			HandleMessage(m);
		}
	}
	
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed:
		    handlePlayerInput(event.key.code, true);
		    break;
		
		case sf::Event::KeyReleased:
		    handlePlayerInput(event.key.code, false);
		    break;
		
		case sf::Event::Closed:
		    mWindow.close();
		    break;
				
		default:
				break;
		}
	}
}

void Game::update(sf::Time elapsedTime)
{
    sf::Vector2f movement(0.f, 0.f);
    if (mIsMovingUp)
        movement.y -= PlayerSpeed;
    if (mIsMovingDown)
        movement.y += PlayerSpeed;
    if (mIsMovingLeft)
        movement.x -= PlayerSpeed;
    if (mIsMovingRight)
        movement.x += PlayerSpeed;

    playerMap_.at(playerName_).sprite.move(movement * elapsedTime.asSeconds());
}

void Game::render()
{
    mWindow.clear();

		std::map<std::string, Player>::iterator it;
		for(it = playerMap_.begin(); it != playerMap_.end(); it++) {
			mWindow.draw(it->second.sprite);
			if(CheckCollisions(it->second) && it->first != playerName_) { 
				Message cmsg(Message::Type::Collision);
				cmsg.AddArg(playerName_);
				cmsg.AddArg(it->first);
				network_->SendTCP(cmsg);
				mWindow.close();
			}
		}

    //mWindow.draw(playerMap_.at(playerName_));
    mWindow.draw(mStatisticsText);
    mWindow.display();
}

void Game::updateStatistics(sf::Time elapsedTime)
{
	mStatisticsUpdateTime += elapsedTime;
	mStatisticsNumFrames += 1;
	
	if (mStatisticsUpdateTime >= sf::seconds(1.0f))
	{
		mStatisticsText.setString(
			"Frames / Second = " + toString(mStatisticsNumFrames) + "\n" +
			"Time / Update = " +
				toString(mStatisticsUpdateTime.asMicroseconds() /
					mStatisticsNumFrames) + "us");
		
		mStatisticsUpdateTime -= sf::seconds(1.0f);
		mStatisticsNumFrames = 0;
	}
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if(key == sf::Keyboard::Escape)
		mWindow.close();

    if (key == sf::Keyboard::W)
        mIsMovingUp = isPressed;
    else if (key == sf::Keyboard::S)
        mIsMovingDown = isPressed;
    else if (key == sf::Keyboard::A)
        mIsMovingLeft = isPressed;
    else if (key == sf::Keyboard::D)
        mIsMovingRight = isPressed;
}

void Game::HandleMessage(Message &msg)
{
	switch(msg.GetType()) {
		case Message::Type::Register:
			AddPlayer(msg);
			break;
		case Message::Type::Unregister:
			RemovePlayer(msg);
			break;
		case Message::Type::Move:
			MovePlayer(msg);
			break;
		case Message::Type::Quit:
			std::cout << "QUITTING" << std::endl;
			mWindow.close();
		default:
			std::cout << msg.GetData() << std::endl;
			break;
	}
}

void Game::AddPlayer(Message &msg)
{
	std::string name = msg.GetArg(0);
	float posX = std::stof(msg.GetArg(1));
	float posY = std::stof(msg.GetArg(2));

	sf::Sprite p;
	p.setTexture(mTexture);
	p.setPosition(posX, posY);

	Player newPlayer;
	newPlayer.sprite = p;
	newPlayer.width = 48;
	newPlayer.height = 64;

	playerMap_[name] = newPlayer;
}

void Game::RemovePlayer(Message &msg)
{
	playerMap_.erase(msg.GetArg(0));
	std::cout << "Size is: " << playerMap_.size() << std::endl;
}

void Game::MovePlayer(Message &msg)
{
	std::string name = msg.GetArg(0);
	float posX = std::stof(msg.GetArg(1));
	float posY = std::stof(msg.GetArg(2));

	if(playerMap_.find(name) != playerMap_.end()) {
		playerMap_.at(name).sprite.setPosition(posX, posY);
	}
}

bool Game::CheckCollisions(Player &p)
{
	float playerLeft, playerRight, playerTop, playerBottom;
	playerLeft = playerMap_.at(playerName_).GetX();
	playerRight = playerMap_.at(playerName_).GetX() + playerMap_.at(playerName_).width;;
	playerTop = playerMap_.at(playerName_).GetY();
	playerBottom = playerMap_.at(playerName_).GetY() + playerMap_.at(playerName_).height;
//	std::cout << "P:" << playerLeft << " " << playerRight << std::endl
//		<< "P:" << playerTop << " " << playerBottom << std::endl;

	float tarLeft, tarRight, tarTop, tarBottom;
	tarLeft = p.GetX();
	tarRight = p.GetX() + p.width;;
	tarTop = p.GetY();
	tarBottom = p.GetY() + p.height;
//	std::cout << "T:" << tarLeft << " " << tarRight << std::endl
//		<< "T:" << tarTop << " " << tarBottom << std::endl;

	bool xColl = playerLeft <= tarRight && playerRight >= tarLeft;
	bool yColl = playerTop <= tarBottom || playerBottom >= tarTop;
	//std::cout << "X:" << xColl << std::endl << "Y:" << yColl << std::endl;
	if(xColl && yColl) { return true; }
	return false;
}

Game::~Game()
{
	if(connected_) {
		delete network_;
	}
}
