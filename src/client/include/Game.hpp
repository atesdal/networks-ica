#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include "ThreadQueue.hpp"

class Network;
class Message;

class Game
{
	public:
    Game(std::string name);
    virtual ~Game();
    void run();

	private:
		struct Player {
			sf::Sprite sprite;
			int width;
			int height;
			float GetX() { return sprite.getPosition().x; }
			float GetY() { return sprite.getPosition().y; }
		};

    void processEvents();
    void update(sf::Time elapsedTime);
    void render();

    void updateStatistics(sf::Time elapsedTime);
    void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);
		void HandleMessage(Message &msg);
		void AddPlayer(Message &msg);
		void RemovePlayer(Message &msg);
		void MovePlayer(Message &msg);
		bool CheckCollisions(Player &p);

    static const float PlayerSpeed;
    static const sf::Time	TimePerFrame;

    sf::RenderWindow mWindow;
    sf::Texture mTexture;
    sf::Font mFont;
    sf::Text mStatisticsText;
    sf::Time mStatisticsUpdateTime;

    std::size_t mStatisticsNumFrames;
    bool mIsMovingUp;
    bool mIsMovingDown;
    bool mIsMovingRight;
    bool mIsMovingLeft;

		bool connected_{ false };
		Network *network_;
		ThreadQueue messageBuffer_;
		std::string playerName_;
		int playerHeight_, playerWidth_;
		std::map<std::string, Player> playerMap_;
};

#endif // GAME_H
