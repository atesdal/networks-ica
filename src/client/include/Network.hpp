#ifndef NETWORK_H
#define NETWORK_H

#include <SFML/Network.hpp>
#include <string>
#include <thread>

class ThreadQueue;
class Message;

class Network
{
	public:
		Network();
    ~Network();

    bool Connect(sf::IpAddress ip, unsigned short int port);
		void Disconnect();
		void Register(std::string name, float posX, float posY);
		void SendTCP(Message &msg);
		void SendUDP(Message &msg);
		void StartReceive(ThreadQueue &messageQ);

		bool IsConnected() const { return connected_; }

	private:
		void ReceiveTCP(ThreadQueue &q);
		void ReceiveUDP(ThreadQueue &q);

  private:
    sf::TcpSocket TCPSock_;
    sf::UdpSocket UDPSock_;
    sf::IpAddress servIP_;
		unsigned int servTCP_, servUDP_;
		std::thread tcpThread_, udpThread_;
		std::string clientName_;
		bool connected_{false};
};

#endif //NETWORK_H

