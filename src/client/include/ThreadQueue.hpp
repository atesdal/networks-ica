#ifndef THREAD_QUEUE_H
#define THREAD_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

#include "Message.hpp"

class ThreadQueue
{
	public:
		ThreadQueue();
		~ThreadQueue();

		void Push(Message &message);
		void Pop(Message &message);
		bool TryPop(Message &message);

	private:
		std::queue<Message> messageBuffer_;
		mutable std::mutex m_;
		std::condition_variable c_;
};
#endif //THREAD_QUEUE_H
