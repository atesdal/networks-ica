#ifndef RETURN_MESSAGE_H
#define RETURN_MESSAGE_H

#include "Message.hpp"

class ReturnMessage : public Message
{
	public:
		ReturnMessage();
		~ReturnMessage();

		void FillMessage(std::string &message);

	private:
		void Format(std::string &rawMessage);
};

#endif //RETURN_MESSAGE_H

