#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <vector>

class Message
{
  public:

		enum Type {
			Register,
			Unregister,
			Move,
			Collision,
			Confirmation,
			Error,
			Quit,
			Whisper,
			Undefined
		};

		Message();
		Message(Type messageType);
		Message(Type messageType, std::string arg);
    virtual ~Message();

		virtual std::string GetData() const;
		Type GetType() const;	
		std::string GetArg(int index);
		std::vector<std::string> GetArgs() const;
		virtual void AddArg(std::string newArg);
	
	protected:
		void ClearArgs();

	protected:
		std::vector<std::string> args_;
		std::string command_;
		Type t_;
		char separator_{ '|' };
		char eol_{ ';' };
};

#endif //MESSAGE_H
