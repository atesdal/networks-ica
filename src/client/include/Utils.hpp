#ifndef UTILS_H
#define UTILS_H

#include <string>

namespace Util
{
	std::string ToString(float toConvert);
}

#endif //UTILS_H

