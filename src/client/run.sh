#!/bin/bash

SFMLPATH=$PWD/../../lib/SFML-2.2/lib/

while [ "$1" != "" ]; do
	case $1 in
		-c )	shift
					export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SFMLPATH
					./sfml_client
					;;
		-s )	shift
					gnome-terminal --working-directory=$PWD/../server/ \
						-- mix run --no-halt
					;;
	esac
	shift
done
